from django.urls import path

from teachers.views.teacher import Index, TeacherDetailsView, TeacherGridJSONData, TeacherImportView

urlpatterns = [
    path('', Index.as_view(), name="teacher"),
    path('teacher-details', TeacherDetailsView.as_view(), name="teacher_details"),
    path(r'data', TeacherGridJSONData.as_view(), name="teacher_grid_data"),
    path(r'import', TeacherImportView.as_view(), name="teacher_import"),
]
