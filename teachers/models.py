from django.conf import settings
from django.db import models


class Teacher(models.Model):
    first_name = models.CharField('First Name', max_length=128)
    last_name = models.CharField('Last Name', max_length=128)
    email = models.EmailField('Email', max_length=256, unique=True)
    phone = models.CharField('Phone Number', max_length=32)
    room_number = models.CharField('Room Number', max_length=32)
    subjects_taught = models.CharField("Subject Taught", max_length=512)
    profile_image = models.ImageField(upload_to="profiles")

    def __str__(self):
        return "{} {} - {}".format(self.first_name, self.last_name, self.email)


class File(models.Model):
    file = models.FileField(
        upload_to=settings.FILE_ROOT
        )