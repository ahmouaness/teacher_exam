import json

from django.contrib.auth.decorators import login_required
from django.forms import model_to_dict
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View
from django_datatables_view.base_datatable_view import BaseDatatableView

from teachers.logic import TeacherCreator
from teachers.models import Teacher


class Index(TemplateView):
    template_name = "index.html"


class TeacherDetailsView(View):
    def get(self, request, *args, **kwargs):
        ctx = {}
        try:
            teacher = Teacher.objects.get(id=request.GET.get('id'))
            teacher = model_to_dict(teacher)
            teacher["subjects_taught"] = teacher["subjects_taught"].split(",")
            ctx['teacher'] = teacher
        except:
            pass
        return render(request, 'view.html', ctx)


class TeacherGridJSONData(BaseDatatableView):
    columns = [
        'id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'room_number',
        'subjects_taught'
    ]

    def get_initial_queryset(self):
        return Teacher.objects.all()

    def get_filter_method(self):
        return self.FILTER_ICONTAINS

    def prepare_results(self, qs):
        data = []
        for item in qs:
            data.append({column: self.render_column(item, column) for column in self._columns})
        return data


@method_decorator(login_required, name="dispatch")
class TeacherImportView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'import.html')

    def post(self,request, *args, **kwargs):

        response = TeacherCreator(self.request).create()
        return HttpResponse(json.dumps(response), content_type="application/json")
