import os
import zipfile
from io import BytesIO

from PIL import Image

import numpy
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.db import transaction
import pandas as pd

from teachers.models import Teacher, File


class TeacherCreator:
    __header = {
        'first name': 'first name',
        'last name': 'last name',
        'profile picture': 'profile picture',
        'email address': 'email address',
        'phone number': 'phone number',
        'room number': 'room number',
        'subjects taught': 'subject taught',
    }

    __required_col = [
        'first name',
        'last name',
        'profile picture',
        'email address',
        'phone number',
        'room number',
        'subjects taught',
    ]

    error_messages = []

    def __init__(self, request):
        self.request = request
        self.error_messages = []

    def fill_header(self, data):
        columns = data.to_dict('split')['columns']
        for c in columns:
            if c.lower() in self.__header:
                self.__header[c.lower()] = c

    def validate_data(self, data):
        rows = data.to_dict('records')

        for index, row in enumerate(rows):
            has_error_empty = False
            for col in self.__required_col:
                try:
                    if numpy.isnan(self.get_data_field(row, col)):
                        self.error_messages.append(
                            f'Row:{index + 2} -> {col.upper()} Required'
                        )
                        has_error_empty = True
                except:
                    pass

            if not has_error_empty:
                email_address = self.get_data_field(row, 'email address').lower()
                try:
                    Teacher.objects.get(email=email_address)
                    self.error_messages.append(
                        f'Row: {index + 2} / {email_address} (Email Address) excite'
                    )

                except:
                    pass

                subjects_taught = self.get_data_field(row, 'subjects taught').split(",")
                if len(subjects_taught) > 5:
                    self.error_messages.append(
                        f'Row: {index + 2} / {email_address} has more than 5 subjects'
                    )

    def get_data_field(self, row, key):
        return row[self.__header[key.lower()]]

    def get_data(self):
        files = self.request.FILES.getlist('csvFile')
        data = []
        images = {}
        for file in files:
            file_model = File.objects.create(
                file=file
            )
            file_name = file_model.file.name
            if file_name.endswith('.csv'):
                data = pd.read_csv(
                    file_model.file.path,
                    encoding='utf-8'
                )
            elif file_name.endswith('.xlsx'):
                data = pd.read_excel(
                    file_model.file.path,
                    encoding='utf-8'
                )
            elif file_name.endswith('.zip'):
                with zipfile.ZipFile(file_model.file.path, 'r') as zip_file:
                    for name in zip_file.namelist():
                        img = zip_file.read(name)
                        try:
                            image = Image.open(BytesIO(img))
                            image.load()
                            image = Image.open(BytesIO(img))
                            image.verify()
                        except ImportError:
                            pass
                        except:
                            continue
                        images.update({name.lower(): img, })

            file_model.delete()

        return data, images

    def create(self):
        data = self.request.FILES
        try:
            if 'csvFile' in data:
                with transaction.atomic():
                    data, images = self.get_data()
                    self.fill_header(data)
                    self.validate_data(data)

                    if self.error_messages:
                        return {
                            'status': 'ERROR',
                            'messages': self.error_messages
                        }

                    data = data.to_dict('records')

                    for row in data:
                        row = {
                            key.strip().lower(): str(value).strip() for key, value in row.items()
                        }
                        teacher = Teacher()
                        teacher.first_name = row["first name"]
                        teacher.last_name = row["last name"]
                        teacher.email = row["email address"]
                        teacher.phone = row["phone number"]
                        teacher.room_number = row["room number"]
                        teacher.subjects_taught = row["subjects taught"]

                        image = images.get(row["profile picture"].lower(), None)
                        if image:
                            name = os.path.split(row["profile picture"].lower())[1]
                            # You now have an image which you can save
                            path = os.path.join(settings.MEDIA_ROOT, "profiles", name)
                            saved_path = default_storage.save(path, ContentFile(image))

                            # image_name = row["profile picture"].lower()
                            teacher.profile_image = saved_path
                        teacher.save()

            return {'status': 'OK', 'message': 'Saved successfully'}
        except Exception as e:
            return {'status': 'ERROR', 'message': f'{e} not found'}
