# README #

Teacher directory Project
 
### Summary  ###
a web application to mange teachers and upload data:
#### Default User: #####
* Teachers List
* User can filter on First Name, Last Name and subjects
* User can view teacher details with profile image
##### Admin User(Logged in user): #####
* Can see same as default user
* Can import data
 
## Uploader summary ##
Oly admin (logged in user) can access to the uploader
> * username: admin
> * password: 1234

User should select 2 files (csv and zip that contains the images)  


* In case of error in data. Errors  will be appear under the submit form and user should correct the data
* No data added to DB in case of error


## Installation ##
run: 
> python3 -m pip install -r requirements.txt
>
> python3 manage.py runserver

Enjoy the web app