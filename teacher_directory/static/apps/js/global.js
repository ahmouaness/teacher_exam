var mainModule = function () {
    var is_open = false,
        openMainModal = function (url, modalSize) {
            if (!is_open) {
                is_open = true;
                $("#main_modal .modal-dialog").removeClass("modal-lg").removeClass("modal-sm");
                $("#main_modal .modal-dialog").addClass(modalSize);
                $("#main_modal .modal-content").load(url, function (response, status, xhr) {
                    if (status != "error") {
                        $('#main_modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $("#main_modal").modal('show');
                        is_open = false;
                        KTApp.initComponents();
                    }
                });
            }
        },

        closeMainModal = function () {
            $("#main_modal").modal('hide');
            $("#main_modal .modal-loader").hide();
            setTimeout(function () {
                $("#main_modal .modal-content").html('');
                $("#main_modal .modal-dialog").removeClass("modal-lg").removeClass("modal-sm");
            }, 500);
        };
    return {
        openModal: openMainModal,
        closeModal: closeMainModal,
    };
}();