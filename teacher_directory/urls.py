from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.static import serve

import teachers.urls
from django.contrib.auth import views as auth_views

from teacher_directory.views import logout_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(teachers.urls)),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout', logout_view, name='logout'),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),

]
