Django==3.1.5
django-datatables-view==1.18.0
Pillow==8.4.0
numpy==1.21.2
pandas==1.3.4